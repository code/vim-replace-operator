replace\_operator.vim
=====================

This plugin provides normal and visual mode mapping targets to replace the text
bounded by a motion with the contents of a register, by default the unnamed
register.

This allows you, for example, to select a set of lines and replace any other
set of lines with it in one repeatable operation.  The text you replace stacks
up in the numbered registers as normal, if you do end up needing it back.

License
-------

Copyright (c) [Tom Ryder][1]. Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
